package com.somobu.retrowaveapi;

import com.somobu.kurabu.Main;
import com.iillyyaa2033.utils.StreamUtils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class RetrowaveApi {

    private static final boolean USE_COOKIES = true;
    private static final String domain = "http://retrowave.ru";
    private static final String endpointV1 = "http://retrowave.ru/api/v1/";
    private static final String USER_AGENT = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:80.0) Gecko/20100101 Firefox/80.0";

    private int cursor = -1;
    private String cookie = null;

    public TrackMeta nextTrack() throws IOException {
        return nextTracks(1)[0];
    }

    public TrackMeta[] nextTracks(int count) throws IOException {
        String url = endpointV1 + "tracks?cursor=" + cursor + "&limit=" + count;
        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
        connection.setRequestProperty("User-Agent", USER_AGENT);
        if (USE_COOKIES && cookie != null) connection.setRequestProperty("Cookie", cookie);

        String rawAnswer = StreamUtils.convertStreamToString(connection.getInputStream());
        Response response = Main.gson.fromJson(rawAnswer, Response.class);

        List<String> cookieFields = connection.getHeaderFields().get("Set-Cookie");
        if (USE_COOKIES && cookieFields != null && cookieFields.size() > 0) {
            String cookieField = cookieFields.get(0);
            cookie = cookieField.substring(0, cookieField.indexOf(';'));
        }

        connection.disconnect();

        if (response.status != 200) {
            throw new IOException("Server returned status " + response.status);
        } else {
            cursor += response.body.cursor;
            return response.body.tracks;
        }
    }

    public static class Response {

        public int status = 0;
        public ResponseBody body = new ResponseBody();
    }

    public static class ResponseBody {

        public int cursor = 0;
        public TrackMeta[] tracks;
    }

    public static class TrackMeta {

        public String id;
        public String title;
        public float duration;

        String streamUrl;
        String artworkUrl;

        @Override
        public String toString() {
            return "TrackMeta{" +
                    "id='" + id + '\'' +
                    ", title='" + title + '\'' +
                    ", duration=" + duration +
                    '}';
        }

        public String getStreamUrl() {
            return domain + streamUrl;
        }

        public String getArtworkUrl() {
            return domain + artworkUrl;
        }
    }

}
