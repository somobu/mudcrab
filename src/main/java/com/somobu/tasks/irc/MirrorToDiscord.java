package com.somobu.tasks.irc;

import com.iillyyaa2033.discord.v8.objects.WebhookData;
import com.somobu.kurabu.Main;
import com.somobu.kurabu.irc.IrcTask;
import com.somobu.kurabu.Log;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("unused")
public class MirrorToDiscord extends IrcTask {

    private final Map<String, String> avatars = getAvatarReplacements();

    @Override
    public void onUserJoin(String channel, String login, String hostname) {
        dmsg(login, "Joined " + channel);
    }

    @Override
    public void onUserLeft(String login, String hostname, String reason) {
        dmsg(login, "Left: " + reason);
    }

    @Override
    public void onUserMessage(String channel, String login, String hostname, String message) {
        dmsg(login, message);
    }

    private Map<String, String> getAvatarReplacements() {
        final Map<String, String> avatars = new HashMap<>();
        avatars.put("somobu", "https://cdn.discordapp.com/avatars/364795733158920193/5718ee8cd01af4339dc143d3655d0284.webp");
        avatars.put("somobu_n", "https://cdn.discordapp.com/avatars/364795733158920193/5718ee8cd01af4339dc143d3655d0284.webp");
        avatars.put("pathetic", "https://cdn.discordapp.com/avatars/413335093046935552/d22a2f9c2e7db0c2da1b9f1c1ee1292e.webp");
        avatars.put("PircBot", "https://cdn.discordapp.com/avatars/593405191995392000/b930688262ac393edbffdaf83158fa34.webp");
        avatars.put("", "");
        return avatars;
    }

    private void dmsg(String usr, String txt) {
        if (Main.discord != null) {
            WebhookData data = new WebhookData();
            data.username = usr;
            data.content = txt;
            data.avatar_url = avatars.get(usr);

            String id = getString("dc_hook_id");
            String token = getString("dc_hook_token");

            try {
                Main.discord.api.executeWebhook(id, token, data);
            } catch (IOException e) {
                Log.e(e);
            }
        }
    }

}
