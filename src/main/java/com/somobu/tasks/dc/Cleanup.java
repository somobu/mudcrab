package com.somobu.tasks.dc;

import com.somobu.Utils;
import com.iillyyaa2033.discord.v8.objects.*;
import com.somobu.kurabu.Const;
import com.somobu.kurabu.dc.DiscordConnection;
import com.somobu.kurabu.dc.DiscordTask;

import java.io.IOException;
import java.util.ArrayList;

@SuppressWarnings("unused")
public class Cleanup extends DiscordTask {

    private static final String[] PREFIXES = {"!\\S.*", ">\\S.*", "~>\\S.*"};

    @Override
    public ApplicationCommand getApplicationCommand() {
        ApplicationCommand command = new ApplicationCommand();
        command.name = "cleanup";
        command.description = "Удалить последние 50 сообщений-команд и сообщений от ботов в канале";
        command.type = 1;
        return command;
    }

    @Override
    public InteractionResponse respondToInteraction(DiscordConnection connection, Interaction event) {

        String content;

        try {
            String channelId = event.channel_id;
            Message[] messages = connection.api.getChannelMessages(event.channel_id, null, null, null, 50);
            ArrayList<String> toDelete = new ArrayList<>();
            ArrayList<String> toDeleteOld = new ArrayList<>();

            boolean hasPermission = event.guild_id.equals(Const.SPOON);
            for (Message m : messages) {
                if (hasPermission) {
                    if (m.author.bot) {
                        long since = System.currentTimeMillis() - Utils.getMillis(m.id);
                        if (since < 7 * 24 * 60 * 60 * 1000) toDelete.add(m.id);
                        else toDeleteOld.add(m.id);
                    } else {
                        for (String regex : PREFIXES) {
                            if (m.content.matches(regex)) {
                                long since = System.currentTimeMillis() - Utils.getMillis(m.id);
                                if (since < 7 * 24 * 60 * 60 * 1000) toDelete.add(m.id);
                                else toDeleteOld.add(m.id);
                                break;
                            }
                        }
                    }
                } else if (m.author.id.equals(connection.user.id)) {
                    long since = System.currentTimeMillis() - Utils.getMillis(m.id);
                    if (since < 14 * 24 * 60 * 60 * 1000) toDelete.add(m.id);
                    else toDeleteOld.add(m.id);
                }
            }

            if (toDelete.size() > 2) {
                if(hasPermission) connection.api.bulkDeleteMessages(channelId, toDelete.toArray(new String[0]));
                else connection.asyncRateLimitedDelete(channelId, toDelete);
            } else {
                connection.asyncRateLimitedDelete(channelId, toDelete);
            }

            connection.asyncRateLimitedDelete(channelId, toDeleteOld);

            content = "Удоляю...";
        } catch (IOException e) {
            e.printStackTrace();
            content = Utils.rand("Случилась ФАТАЛЬНЯ ошибка!", "Я чуть не упал");
        }

        InteractionResponse response = new InteractionResponse();
        response.type = 4;
        response.data = new InteractionCallbackData();
        response.data.content = content;

        return response;
    }
}
