package com.somobu.tasks.dc;

import com.somobu.Utils;
import com.somobu.kurabu.dc.DiscordConnection;
import com.somobu.kurabu.dc.DiscordServer;
import com.somobu.kurabu.dc.DiscordTask;

import java.io.IOException;
import java.util.Calendar;

@SuppressWarnings("unused")
public class NicknameSaturday extends DiscordTask {

    @Override
    public long getLaunchTimeAfter(DiscordConnection connection, DiscordServer server, long millis) {
        return Utils.getNextDayOfWeekSince(millis, Calendar.SATURDAY, 0, 10);
    }

    @Override
    public void onScheduled(DiscordConnection connection, DiscordServer server) throws IOException {
        connection.api.modifyCurrentUserNick(server.id, Utils.rand(
                "Тот-кого-нельзя-забывать",
                "Тот-кого-нельзя-обижать",
                "Единственный-кто-стоит-там",
                "Остреца",
                "Инкогнито",
                "Кпт Трищенко"
        ));

        /*
                        Main.setGameStatus(Utils.rand(
                        "В гостях у Тельвани",
                        "Шашлыки на Аскадианских о-вах",
                        "Ищем М'Айка Лжеца"
                ));
         */
    }
}
