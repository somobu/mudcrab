package com.somobu.tasks.dc;

import com.iillyyaa2033.discord.v8.objects.*;
import com.somobu.kurabu.dc.DiscordConnection;
import com.somobu.kurabu.dc.DiscordTask;

@SuppressWarnings("unused")
public class Minesweeper extends DiscordTask {

    @Override
    public ApplicationCommand getApplicationCommand() {
        ApplicationCommand command = new ApplicationCommand();
        command.name = "minesweeper";
        command.description = "Сапер";
        command.type = 1;

        ApplicationCommand.Option width = new ApplicationCommand.Option();
        width.type = 4;
        width.required = false;
        width.name = "w";
        width.description = "Ширина поля";

        ApplicationCommand.Option height = new ApplicationCommand.Option();
        height.type = 4;
        height.required = false;
        height.name = "h";
        height.description = "Высота поля";

        ApplicationCommand.Option mines = new ApplicationCommand.Option();
        mines.type = 4;
        mines.required = false;
        mines.name = "mines";
        mines.description = "Количество мин";

        command.options = new ApplicationCommand.Option[]{width, height, mines};

        return command;
    }

    @Override
    public InteractionResponse respondToInteraction(DiscordConnection connection, Interaction event) {

        int x = 10;
        int y = 10;
        int mines = 10;

        if (event.data.options != null) {
            for (Interaction.ApplicationCommandInteractionDataOption option : event.data.options) {
                if ("w".equals(option.name)) x = option.value.getAsInt();
                else if ("h".equals(option.name)) y = option.value.getAsInt();
                else if ("mines".equals(option.name)) mines = option.value.getAsInt();
            }
        }

        x = x > 0 ? x : 1;
        y = y > 0 ? y : 1;
        mines = mines > 0 ? mines : 1;

        int cellCount = x * y;
        if (mines > cellCount) mines = cellCount - 1;

        String content;
        if (cellCount >= 15 * 14) content = "Многовато будед";
        else content = generate(y, x, mines, cellCount > (12 * 12));

        InteractionResponse response = new InteractionResponse();
        response.type = 4;
        response.data = new InteractionCallbackData();
        response.data.content = content;

        return response;
    }


    private static String generate(int rows, int columns, int mines, boolean compact) {
        int[][] field = new int[rows][columns];

        for (int y = 0; y < rows; y++) {
            for (int x = 0; x < columns; x++) {
                field[y][x] = 0;
            }
        }

        for (int i = 0; i < mines; i++) {
            int x = (int) (Math.random() * columns);
            int y = (int) (Math.random() * rows);

            if (field[y][x] == -1) {
                i--;
            } else {
                field[y][x] = -1;

                increaseNotBomb(field, x - 1, y - 1);
                increaseNotBomb(field, x - 1, y);
                increaseNotBomb(field, x - 1, y + 1);

                increaseNotBomb(field, x, y - 1);
                increaseNotBomb(field, x, y);
                increaseNotBomb(field, x, y + 1);

                increaseNotBomb(field, x + 1, y - 1);
                increaseNotBomb(field, x + 1, y);
                increaseNotBomb(field, x + 1, y + 1);
            }
        }

        StringBuilder sb = new StringBuilder();
        for (int y = 0; y < rows; y++) {
            for (int x = 0; x < columns; x++) {
                String ending = x == columns - 1 ? " ||" : " || ";
                sb.append(compact ? "||" : "|| ");
                sb.append(fromCode(field[y][x]));
                sb.append(compact ? "||" : ending);
            }
            if (y < rows - 1) sb.append("\n");
        }
        return sb.toString();
    }

    private static void increaseNotBomb(int[][] field, int x, int y) {
        if (y < 0 || y >= field.length) return;
        if (x < 0 || x >= field[y].length) return;
        if (field[y][x] == -1) return;

        field[y][x]++;
    }

    private static String fromCode(int code) {
        switch (code) {
            case -1:
                return "\uD83D\uDCA3";
            case 0:
                return "0️⃣";
            case 1:
                return "1️⃣";
            case 2:
                return "2️⃣";
            case 3:
                return "3️⃣";
            case 4:
                return "4️⃣";
            case 5:
                return "5️⃣";
            case 6:
                return "6️⃣";
            case 7:
                return "7️⃣";
            case 8:
                return "8️⃣";
        }
        return "❓";
    }

}
