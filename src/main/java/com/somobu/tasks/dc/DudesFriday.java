package com.somobu.tasks.dc;

import com.somobu.Utils;
import com.somobu.kurabu.dc.DiscordConnection;
import com.somobu.kurabu.dc.DiscordServer;
import com.somobu.kurabu.dc.DiscordTask;

import java.io.IOException;
import java.util.Calendar;

@SuppressWarnings("unused")
public class DudesFriday extends DiscordTask {

    @Override
    public long getLaunchTimeAfter(DiscordConnection connection, DiscordServer server, long millis) {
        return Utils.getNextDayOfWeekSince(millis, Calendar.FRIDAY, 9, 0);
    }

    @Override
    public void onScheduled(DiscordConnection connection, DiscordServer server) throws IOException {
        connection.postPicture(server.main_channel, Utils.rand(
                "dudes/dudes_friday.png",
                "taurine/momiji_friday.png",
                "taurine/momiji_friday_2.jpeg",
                "taurine/momiji_friday_3.jpeg",
                "taurine/momiji_friday_4.jpeg",
                "taurine/momiji_friday_5.jpeg",
                "taurine/momiji_friday_6.jpeg",
                "taurine/momiji_friday_7.jpeg"
        ));
    }
}
