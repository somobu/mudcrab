package com.somobu.tasks.dc;

import com.somobu.Utils;
import com.iillyyaa2033.discord.v8.objects.Message;
import com.somobu.kurabu.dc.DiscordConnection;
import com.somobu.kurabu.dc.DiscordServer;
import com.somobu.kurabu.dc.DiscordTask;

import java.io.IOException;

@SuppressWarnings("unused")
public class CrabMentioned extends DiscordTask {

    private static final int REACTION_DEFAULT = 0;
    private static final int REACTION_QUESTION = 1;
    private static final int REACTION_EMOTIONAL = 2;

    @Override
    public boolean handleBotMessages() {
        return true;
    }

    @Override
    public void onMessage(DiscordConnection connection, DiscordServer server, Message message) throws IOException {

        if (message.content != null && message.content.contains(connection.user.id)) {
            String answer;

            switch (selectReaction(message.content)) {
                case REACTION_QUESTION:
                    answer = Utils.rand(
                            "Да не знаю я! Отвяжись!",
                            "Так много вопросов. Так мало ответов",
                            "Ы?",
                            "*прячет глаза*",
                            "*щелкает клешнями*"
                    );
                    break;
                case REACTION_EMOTIONAL:
                    answer = Utils.rand(
                            "Согласны?)0",
                            "Е!!",
                            "*ворчит*",
                            "Тррр"
                    );
                    break;
                case REACTION_DEFAULT:
                default:
                    answer = Utils.rand(
                            "Не пингуй меня больше!",
                            "Да-да?",
                            "Под этим солнцем и небом мы приветствуем тебя!",
                            "Приближается время молитвы",
                            "Три бога. Одна истинная вера!",
                            "Ничтожество!",
                            "Мы следим за тобой... Ничтожество!",
                            "Не, ну это бан"
                    );
            }

            if (answer != null) {
                connection.postMessage(message.channel_id, answer);
            }
        }
    }

    private int selectReaction(String message) {
        if (message.endsWith("?")) return REACTION_QUESTION;
        if (message.endsWith("!")) return REACTION_EMOTIONAL;

        return REACTION_DEFAULT;
    }
}
