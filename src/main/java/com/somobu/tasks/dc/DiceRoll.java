package com.somobu.tasks.dc;

import com.somobu.Utils;
import com.iillyyaa2033.discord.v8.objects.*;
import com.somobu.kurabu.dc.DiscordConnection;
import com.somobu.kurabu.dc.DiscordServer;
import com.somobu.kurabu.dc.DiscordTask;

import java.io.IOException;

@SuppressWarnings("unused")
public class DiceRoll extends DiscordTask {

    private static final int MAX_DICE = 1500;
    private static final int MAX_ROLLS = 50;
    private static final String defaultDice = "\\d+d\\d+";

    private static final String[] names = new String[]{"dice", "дайс", "roll", "ролл"};

    @Override
    public void onMessage(DiscordConnection connection, DiscordServer server, Message message) throws IOException {
        if (message.content == null) return;

        String command = null;

        for (String name : names) {
            if (message.content.startsWith("!" + name)) {
                command = name;
                break;
            }
        }

        if (command == null) return;


        MessageJson json = new MessageJson();
        String proper = message.content.substring(message.content.indexOf(command) + command.length()).trim();

        if (proper.matches(defaultDice)) {
            String[] a = proper.split("d");

            try {
                int rolls = Integer.parseInt(a[0]);
                int dice = Integer.parseInt(a[1]);

                json = roll(rolls, dice);
            } catch (Exception e) {
                json.content = "Не удалось опознать дайс :/";
            }
        } else {
            json.content = "Не смог понять команду. Правильно роллить дайс так: `!roll 1d6`";
        }

        connection.postMessage(message.channel_id, json);
    }

    @Override
    public ApplicationCommand getApplicationCommand() {
        ApplicationCommand command = new ApplicationCommand();
        command.name = "roll";
        command.description = "Бросок дайса, 1d6 по дефолту";
        command.type = 1;

        ApplicationCommand.Option optionCount = new ApplicationCommand.Option();
        optionCount.type = 4;
        optionCount.required = false;
        optionCount.name = "rolls";
        optionCount.description = "Количество бросков";

        ApplicationCommand.Option optionDice = new ApplicationCommand.Option();
        optionDice.type = 4;
        optionDice.required = false;
        optionDice.name = "dice";
        optionDice.description = "Количество граней дайса";

        command.options = new ApplicationCommand.Option[]{optionCount, optionDice};

        return command;
    }

    @Override
    public InteractionResponse respondToInteraction(DiscordConnection connection, Interaction event) {

        int rolls = 1;
        int dice = 6;

        if (event.data.options != null) {
            for (Interaction.ApplicationCommandInteractionDataOption option : event.data.options) {
                if ("rolls".equals(option.name)) rolls = option.value.getAsInt();
                else if ("dice".equals(option.name)) dice = option.value.getAsInt();
            }
        }

        MessageJson json = roll(rolls, dice);

        InteractionResponse response = new InteractionResponse();
        response.type = 4;
        response.data = new InteractionCallbackData();
        response.data.content = json.content;
        response.data.embeds = new Embed[]{json.embed};

        return response;
    }

    private MessageJson roll(int rolls, int dice) {
        MessageJson json = new MessageJson();

        if (dice < 2 || dice > MAX_DICE) {
            json.content = Utils.rand(
                    "Какой-то странный дайс",
                    "А что с дайсом?",
                    "Дайс неправильный!",
                    "Неевклидовы кости запрещены");
        } else if (rolls <= 0 || rolls > MAX_ROLLS) {
            json.content = Utils.rand("Не, столько роллов я не вытяну", "С роллами перебор");
        } else {
            StringBuilder rollResults = new StringBuilder();

            int sum = 0;
            for (int i = 0; i < rolls; i++) {
                int ceil = (int) Math.ceil(Math.random() * dice);
                sum += ceil;

                if (rolls > 5 && i % 5 == 0) {
                    int cnt = i / 5 + 1;
                    rollResults.append("\n");
                    rollResults.append(cnt < 10 ? " " : "");
                    rollResults.append(cnt);
                    rollResults.append(".");
                }

                if (rolls < 6) {
                    if (i > 0)
                        rollResults.append(" ");
                } else {
                    if (ceil < 10) rollResults.append("    ");
                    else if (ceil < 100) rollResults.append("   ");
                    else if (ceil < 1000) rollResults.append("  ");
                    else rollResults.append(" ");
                }
                rollResults.append(ceil);
            }

            json.embed = new Embed();
            json.embed.title = "Роллим " + rolls + "d" + dice;
            json.embed.description = "Роллы:\n```" + rollResults.toString() + "```";
            json.embed.footer = new EmbedFooter();
            json.embed.footer.text = "Сумма: " + sum;
        }

        return json;
    }
}
