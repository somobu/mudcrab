package com.somobu.tasks.dc;

import com.somobu.Utils;
import com.iillyyaa2033.discord.v8.objects.Message;
import com.somobu.kurabu.dc.DiscordConnection;
import com.somobu.kurabu.dc.DiscordServer;
import com.somobu.kurabu.dc.DiscordTask;

import java.io.IOException;

@SuppressWarnings("unused")
public class Reaction extends DiscordTask {

    @Override
    public void onMessage(DiscordConnection connection, DiscordServer server, Message message) throws IOException {

        String content = "" + message.content;
        if ("true".equalsIgnoreCase(getString("lowercase"))) content = content.toLowerCase();

        String[] includeOn = fromJson("include_on", String[].class);

        float randomThreshold = 1.0f;
        try {
            randomThreshold = Float.parseFloat(getString("random_threshold"));
        } catch (Exception ignored) {

        }

        String[] responses = fromJson("responses", String[].class);
        String[] pics = fromJson("pics", String[].class);

        if (contains(content, includeOn)) {
            if (Math.random() < randomThreshold) {
                if (responses != null) {
                    connection.postMessage(message.channel_id, Utils.rand(responses));
                } else if (pics != null) {
                    connection.postPicture(message.channel_id, Utils.rand(pics));
                } else {
                    connection.postMessage(message.channel_id, Utils.rand("Щелкает клешнями"));
                }
            }
        }

    }

    boolean contains(String str, String... subsrs) {
        if (str == null) return false;

        for (String sb : subsrs) {
            if (str.contains(sb)) return true;
        }

        return false;
    }
}
