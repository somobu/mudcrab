package com.somobu.tasks.dc;

import com.iillyyaa2033.discord.v8.objects.Message;
import com.somobu.Utils;
import com.somobu.kurabu.dc.DiscordConnection;
import com.somobu.kurabu.dc.DiscordServer;
import com.somobu.kurabu.dc.DiscordTask;

import java.io.IOException;

@SuppressWarnings("unused")
public class Awoo extends DiscordTask {

    String[] pics = new String[]{
            "awoo/awoo.png",
            "awoo/awoo_2.png",
            "awoo/awoo_3.png",
            "awoo/awoo_4.png",
            "awoo/awoo_5.png"
    };

    @Override
    public void onMessage(DiscordConnection connection, DiscordServer server, Message message) throws IOException {
        String content = "" + message.content;

        if (content.toLowerCase().matches("^([aа]((woo+)|(ву+))).*")) {
            connection.postPicture(message.channel_id, Utils.rand(pics));
        }
    }

}
