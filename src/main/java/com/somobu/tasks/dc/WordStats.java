package com.somobu.tasks.dc;

import com.somobu.Utils;
import com.iillyyaa2033.discord.v8.objects.*;
import com.somobu.kurabu.Main;
import com.somobu.kurabu.dc.DiscordConnection;
import com.somobu.kurabu.dc.DiscordServer;
import com.somobu.kurabu.dc.DiscordTask;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings("unused")
public class WordStats extends DiscordTask {

    @Override
    public void onMessage(DiscordConnection connection, DiscordServer server, Message message) throws IOException {

        int count = countWordsUsingSplit(message.content);
        String sid = message.guild_id;
        String author = message.author.id;
        String uname = message.author.username;

        Main.db.updateWordCounterToday(sid, author, uname, count);
        Main.db.updateWordCounterTotal(sid, author, uname, count);

    }

    private int countWordsUsingSplit(String input) {
        if (input == null || input.isEmpty()) {
            return 0;
        }

        String[] words = input.split("\\s+");
        return words.length;
    }

    @Override
    public ApplicationCommand getApplicationCommand() {
        ApplicationCommand command = new ApplicationCommand();
        command.name = "top";
        command.description = "Топ юзеров по написанным словам";
        command.type = 1;

        ApplicationCommand.Option optionAll = new ApplicationCommand.Option();
        optionAll.type = 5;
        optionAll.required = false;
        optionAll.name = "all";
        optionAll.description = "Вывести пользователей за все время наблюдения";

        command.options = new ApplicationCommand.Option[]{optionAll};

        return command;
    }

    @Override
    public InteractionResponse respondToInteraction(DiscordConnection connection, Interaction event) {

        String sid = event.guild_id;

        boolean all = false;

        if (event.data.options != null) {
            for (Interaction.ApplicationCommandInteractionDataOption option : event.data.options) {
                if ("all".equals(option.name)) {
                    all = option.value.getAsBoolean();
                }
            }
        }

        String content;
        if (all) {
            int i = (int) (Math.random() * 11);

            if (i == 0) {
                content = Utils.rand("Топ-топ-топ", "Тыгыдык!");
            } else {
                String top = Main.db.getPrettyWordCounterTop(sid);
                content = "Топ за все время:\n" + top;
            }
        } else {
            int j = (int) (Math.random() * 11);

            if (j == 0) {
                content = Utils.rand("Топ-топ-топ", "Тыгыдык!");
            } else {
                String top = Main.db.getPrettyWordCounterToday(sid);
                String newstring = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date(System.currentTimeMillis()));

                content = "Топ за сегодня (" + newstring + "):\n" + top;
            }
        }

        InteractionResponse response = new InteractionResponse();
        response.type = 4;
        response.data = new InteractionCallbackData();
        response.data.content = content;

        return response;
    }
}
