package com.somobu.tasks.dc;

import com.iillyyaa2033.discord.v8.gateway_objs.MessageDeleted;
import com.iillyyaa2033.discord.v8.objects.Message;
import com.somobu.kurabu.dc.DiscordConnection;
import com.somobu.kurabu.dc.DiscordServer;
import com.somobu.kurabu.dc.DiscordTask;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
public class DelMod extends DiscordTask {

    private static final int QUEUE_SIZE = 256;

    List<Message> lastMsgs = new ArrayList<>();

    Message getMsg(String id) {
        for (Message msg : lastMsgs) {
            if (msg.id.equals(id)) {
                return msg;
            }
        }
        return null;
    }

    @Override
    public void onMessage(DiscordConnection connection, DiscordServer server, Message message) throws IOException {
        lastMsgs.add(message);
        if (lastMsgs.size() > QUEUE_SIZE) lastMsgs.remove(0);
    }

    @Override
    public void onMessageDelete(DiscordConnection connection, DiscordServer server, MessageDeleted message) throws IOException {
        Message msg = getMsg(message.id);
        if (msg != null && server.sys_channel != null) {
            connection.postMessage(
                    server.sys_channel,
                    "Recent message" +
                            " by " + msg.author.username + "#" + msg.author.discriminator +
                            " in <#" + msg.channel_id + ">" +
                            " was deleted"
            );
        }
    }
}
