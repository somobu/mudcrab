package com.somobu.tasks.dc.voice;

import com.iillyyaa2033.discord.v8.objects.ApplicationCommand;
import com.iillyyaa2033.discord.v8.objects.Interaction;
import com.iillyyaa2033.discord.v8.objects.InteractionResponse;
import com.somobu.kurabu.dc.DiscordConnection;
import com.somobu.kurabu.dc.DiscordTask;
import com.somobu.kurabu.dcvoice.VoiceMgr;

@SuppressWarnings("unused")
public class Queue extends DiscordTask {

    final int nameCap = 35;

    @Override
    public ApplicationCommand getApplicationCommand() {
        ApplicationCommand command = new ApplicationCommand();
        command.name = "queue";
        command.description = "Очередь воспроизведения";
        command.type = 1;
        return command;
    }

    @Override
    public InteractionResponse respondToInteraction(DiscordConnection connection, Interaction event) {


        String userVoice = connection.voiceMgr.getChannelOfUser(event.guild_id, event.member.user.id);
        if (userVoice == null) {
            return r("Error: unable to locate user's voice channel");
        }

        VoiceMgr.VoiceWrapper vw = connection.voiceMgr.getVoice(event.guild_id);
        if (vw == null) {
            return r("Error: bot is not in a voice channel right now");
        }

        if (vw.playlist.entries.size() > 0) {
            String content = "Playlist for <#" + vw.channel + ">:\n" + "```\n";
            for (String s : vw.playlist.simpleNames()) {
                content += "- " + (s.length() > nameCap ? s.substring(0, nameCap) : s);
                content += "\n";
            }
            content += "```";

            return r(content);
        } else {
            return r("Error: it seems that bot is not playing anything");
        }
    }

}
