package com.somobu.tasks.dc.voice;

import com.iillyyaa2033.discord.v8.objects.ApplicationCommand;
import com.iillyyaa2033.discord.v8.objects.Interaction;
import com.iillyyaa2033.discord.v8.objects.InteractionResponse;
import com.somobu.kurabu.Main;
import com.somobu.kurabu.dc.DiscordConnection;
import com.somobu.kurabu.dc.DiscordTask;
import com.somobu.kurabu.dcvoice.VoiceMgr;

@SuppressWarnings("unused")
public class Leave extends DiscordTask {

    @Override
    public ApplicationCommand getApplicationCommand() {
        ApplicationCommand command = new ApplicationCommand();
        command.name = "leave";
        command.description = "Выгоняет бота из войса";
        command.type = 1;
        return command;
    }

    @Override
    public InteractionResponse respondToInteraction(DiscordConnection connection, Interaction event) {

        String content = "Attempting to leave...";

        VoiceMgr.VoiceWrapper vw = connection.voiceMgr.getVoice(event.guild_id);
        if (vw == null) {
            content += "\nError: it seems bot is not in voice channel right now";
            return r(content);
        }

        try {
            connection.voiceMgr.leave(event.guild_id);
            content += "\nLeft voice channel";
        } catch (Exception e) {
            content += "\nFailed to left channel: " + e.getMessage();
            Main.e(e);
        }

        return r(content);
    }

}
