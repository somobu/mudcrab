package com.somobu.kurabu.dc;

import com.iillyyaa2033.discord.v8.DiscordGateway;
import com.iillyyaa2033.discord.v8.DiscordHttp;
import com.iillyyaa2033.discord.v8.gateway_objs.MessageDeleted;
import com.iillyyaa2033.discord.v8.gateway_objs.Ready;
import com.iillyyaa2033.discord.v8.gateway_objs.UpdateStatus;
import com.iillyyaa2033.discord.v8.objects.*;
import com.somobu.kurabu.Cfg;
import com.somobu.kurabu.Log;
import com.somobu.kurabu.Main;
import com.somobu.kurabu.Necron;
import com.somobu.kurabu.dcvoice.VoiceMgr;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class DiscordConnection {

    /**
     * Cached pictures may be deleted, so we'll keep posted pic url in cache 2h max
     */
    private static final long PIC_CACHE_TIME = 2 * 60 * 60 * 1000;

    private static final boolean DEBUG = true;

    public User user;
    public DiscordHttp api;
    public DiscordGateway realtime;
    public VoiceMgr voiceMgr;

    public Map<String, DiscordServer> configs = new HashMap<>();

    private final ArrayBlockingQueue<Runnable> queue = new ArrayBlockingQueue<>(128);
    private final ThreadPoolExecutor executor = new ThreadPoolExecutor(1, 4, 2, TimeUnit.MINUTES, queue);

    public String currentStatus = "TES III: Morrowind";
    public Map<String, String> pics2urls = new HashMap<>();
    public Map<String, Long> caches2times = new HashMap<>();
    public Map<String, DiscordTask> interactions2tasks = new HashMap<>();

    public DiscordConnection(String discord_token) {
        api = new DiscordHttp(discord_token);
        realtime = new DiscordGateway(discord_token);
        realtime.setDiscordListener(new DiscordListener());
        realtime.setStateListener(new StateListener());
        voiceMgr = new VoiceMgr(this);
    }

    public void loadConfigs() throws Exception {
        user = api.getCurrentUser();
        if (user == null) throw new RuntimeException("Unable to get current user from Discord");

        File dcCfgFile = new File(Main.RES + "/../cfgs/dc/");
        for (File file : dcCfgFile.listFiles()) {
            String key = file.getName().replace(".json", "");

            DiscordServer server = DiscordServer.fromFile(file);
            server.id = key;

            if (server.tasks != null) Cfg.instantiateTasks(server.tasks, server.actual_tasks);
            else server.actual_tasks = new ArrayList<>();

            for (DiscordTask task : server.actual_tasks) task.guildId = key;

            configs.put(key, server);
        }
    }

    public void reloadCommands() throws IOException, InterruptedException {
        long delay = 1000;

        for (String guildId : configs.keySet()) {
            DiscordServer server = configs.get(guildId);
            if (server.suppress_cmd_loading) continue;

            List<ApplicationCommand> commands = new ArrayList<>();

            for (DiscordTask task : server.actual_tasks) {
                ApplicationCommand command = task.getApplicationCommand();
                if (command != null) {
                    commands.add(command);
                    interactions2tasks.put(getInteractionId(guildId, command.name), task);
                }
            }

            api.bulkOverrideGuildApplicationCommands(user.id, guildId, commands.toArray(new ApplicationCommand[0]));
            Thread.sleep(delay);
        }
    }

    public void start() {
        realtime.setInitialPresence(UpdateStatus.customStatus("online", false, System.currentTimeMillis(),
                "Look I'm a bot!", null));
        realtime.start();
    }

    public void postMessage(String channelId, String message) throws IOException {
        MessageJson json = new MessageJson();
        json.content = message;
        postMessage(channelId, json);
    }

    public void postMessage(String channelId, MessageJson json) throws IOException {
        api.createMessage(channelId, json);
    }

    public synchronized void postPicture(String channelId, String picRes) throws IOException {
        String cachedUrl = pics2urls.get(picRes);
        Long cachedTime = caches2times.get(picRes);
        boolean forcePost = cachedTime == null || System.currentTimeMillis() - cachedTime > PIC_CACHE_TIME;

        if (cachedUrl == null || forcePost) {
            String fullPath = Main.RES + "/pics/" + picRes;
            Message message = api.createMessage(
                    channelId,
                    new MessageMultipart(),
                    fullPath
            );

            if (message.attachments != null && message.attachments.length == 1) {
                Attachment m = message.attachments[0];
                pics2urls.put(picRes, m.url);
                caches2times.put(picRes, System.currentTimeMillis());
            }

        } else {
            postMessage(channelId, cachedUrl);
        }
    }

    public void asyncRateLimitedDelete(final String channelId, final List<String> toDelete) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                rateLimitedDelete(channelId, toDelete);
            }
        });
    }

    public synchronized void rateLimitedDelete(String channelId, List<String> toDelete) {
        for (String id : toDelete) {
            try {
                api.deleteMessage(channelId, id);
            } catch (IOException e) {
                Main.e(e);
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                Main.e(e);
            }
        }
    }

    public long handleNecron(long launchTime) {

        long minValidTime = Long.MAX_VALUE;

        for (final DiscordServer server : configs.values()) {
            for (final DiscordTask task : server.actual_tasks) {

                long time = task.getLaunchTimeAfter(DiscordConnection.this, server, launchTime - Necron.FIRE_THRESHOLD_MS / 2);

                if (time > launchTime - Necron.FIRE_THRESHOLD_MS / 2) {

                    if (time < launchTime + Necron.FIRE_THRESHOLD_MS / 2) {
                        executor.execute(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    task.onScheduled(DiscordConnection.this, server);
                                } catch (IOException e) {
                                    Main.e(e);
                                }
                            }
                        });

                        time = task.getLaunchTimeAfter(DiscordConnection.this, server, launchTime + Necron.FIRE_THRESHOLD_MS);
                        time = Math.max(Necron.FIRE_THRESHOLD_MS, time);
                    }

                    minValidTime = Math.min(time, minValidTime);
                }

            }
        }

        return minValidTime;
    }

    private String getInteractionId(String guild, String name) {
        return guild + ":" + name;
    }

    private class DiscordListener extends DiscordGateway.EventsListener {

        @Override
        public void onReady(Ready ready) {
            long since = System.currentTimeMillis();

            UpdateStatus status = new UpdateStatus();
            status.status = "online";
            status.afk = true;
            status.since = since;

            Activity activity = new Activity();
            activity.type = 0;
            activity.name = "TES III: Morrowind";

            status.activities = new Activity[]{activity};

            realtime.updatePresence(status);
        }

        @Override
        public void onGuildCreate(Guild guild) {
            if (configs.get(guild.id) == null) {
                DiscordServer server = new DiscordServer();
                configs.put(guild.id, server);
                Main.d("Added config for guild " + guild.name + " (id " + guild.id + ")");
            }
        }


        @Override
        public void onMessageCreate(final Message message) {
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    DiscordServer server = configs.get(message.guild_id);
                    boolean isBot = message.author != null && message.author.bot;

                    if (server == null) {
                        Main.d("Unable find config for guild " + message.guild_id);
                    } else {
                        for (DiscordTask task : server.actual_tasks) {
                            try {
                                boolean botGuard = !isBot || task.handleBotMessages();
                                if (botGuard) {
                                    task.onMessage(DiscordConnection.this, server, message);
                                }
                            } catch (IOException e) {
                                Main.e(e);
                            }
                        }
                    }
                }
            });
        }

        @Override
        public void onMessageDelete(final MessageDeleted message) {
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    DiscordServer server = configs.get(message.guild_id);

                    if (server == null) {
                        Main.d("Unable find config for guild " + message.guild_id);
                    } else {
                        for (DiscordTask task : server.actual_tasks) {
                            try {
                                task.onMessageDelete(DiscordConnection.this, server, message);
                            } catch (IOException e) {
                                Main.e(e);
                            }
                        }
                    }
                }
            });
        }

        @Override
        public void onVoiceStateUpdate(final VoiceState voiceState) {
            voiceMgr.setVoiceOfUser(voiceState.guild_id, voiceState.user_id, voiceState.channel_id);

            executor.execute(new Runnable() {
                @Override
                public void run() {
                    DiscordServer server = configs.get(voiceState.guild_id);
                    if (server == null) {
                        Main.d("Unable find config for guild " + voiceState.guild_id);
                    } else {
                        for (DiscordTask task : server.actual_tasks) {
                            task.onVoiceStateUpdate(DiscordConnection.this, server, voiceState);
                        }
                    }
                }
            });
        }


        @Override
        public void onInteractionCreate(final Interaction event) {
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    DiscordTask task = interactions2tasks.get(getInteractionId(event.guild_id, event.data.name));
                    if (task == null) throw new RuntimeException("Unable to find interaction event.data.id");

                    try {
                        InteractionResponse response = task.respondToInteraction(DiscordConnection.this, event);
                        if (response == null) {
                            throw new RuntimeException("Task " + task.getClass() + " hasn't responded to interaction");
                        }

                        api.respondInteraction(event.id, event.token, response);
                    } catch (Exception e) {
                        Main.e(e);
                    }
                }
            });
        }
    }

    private class StateListener implements DiscordGateway.StateListener {

        @Override
        public void onConnected() {

        }

        @Override
        public void onReconnected() {
            if (DEBUG) Log.d("Realtime: Reconnected");
        }

        @Override
        public void onError(String s) {
            if (DEBUG) Log.d("Realtime: Error " + s);
        }

        @Override
        public void onStopped() {
            Main.e(new Exception("Realtime stopped -- check that wether you have to fix is"));
        }

    }

}
