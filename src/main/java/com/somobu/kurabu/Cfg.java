package com.somobu.kurabu;

import com.google.gson.JsonElement;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Map;

public class Cfg {

    public static final String TASK_CLASSNAME_PREFIX = "com.somobu.tasks";

    public static class BotConfig {

        public String instance_name = "Mudcrab test";

        public Discord discord = null;
        public Irc irc = null;

    }

    public static class Discord {
        public String token = null;

        /**
         * ID of channel to post log messages to
         */
        public String system_log_channel = null;
    }

    public static class Irc {
        public String ip;
        public int port;
        public String login;
        public String password;

        public Task[] tasks;
    }

    public static class Task {

        @SerializedName("class") public String className = "";
        public Map<String, JsonElement> params = null;

    }

    public static <T extends com.somobu.kurabu.Task> void instantiateTasks(Task[] configs, List<T> instances) {
        instances.clear();

        for (Task cfg : configs) {
            try {
                String className = cfg.className;
                if (className.startsWith(".")) className = TASK_CLASSNAME_PREFIX + className;

                T job = (T) Class.forName(className).newInstance();

                if (cfg.params != null) {
                    for (String key : cfg.params.keySet()) job.set(key, cfg.params.get(key));
                }

                instances.add(job);
            } catch (Exception e) {
                Main.e(e);
            }
        }
    }
}
