package com.somobu.kurabu.dcvoice;

import com.google.gson.JsonObject;
import com.somobu.retrowaveapi.RetrowaveApi;

import javax.sound.sampled.AudioInputStream;
import java.io.IOException;

public class Providers {

    public static final int PROVIDER_FILE = 0;
    public static final int PROVIDER_YOUTUBE = 1;
    public static final int PROVIDER_RETROWAVE = 2;

    public static AudioInputStream launchConverter(Playlist.Entry entry, MediaConverter converter)
            throws IOException, InterruptedException {

        switch (entry.provider) {
            case PROVIDER_YOUTUBE:
                String yt_ref = entry.data.get("ref").getAsString();
                if (entry.data.get("search").getAsBoolean()) {
                    return converter.launchYTDLtoFfmpegToConverter("ytsearch1:" + yt_ref);
                } else {
                    return converter.launchYTDLtoFfmpegToConverter(yt_ref);
                }
            case PROVIDER_RETROWAVE:
                return converter.launchFfmpegFileConverter(entry.data.get("streamUrl").getAsString());
            default:
                return null;
        }
    }

    public static final int LIST_MGR_NONE = 0;
    public static final int LIST_MGR_RETROWAVE = 1;

    public static void managePlaylist(Playlist playlist) throws IOException {
        switch (playlist.playlist_manager) {
            case LIST_MGR_RETROWAVE:
                manageRW(playlist);
                break;
        }
    }

    public static void manageRW(Playlist playlist) throws IOException {
        int addCount = 10 - playlist.entries.size();
        if (addCount > 0) {
            RetrowaveApi.TrackMeta[] meta = new RetrowaveApi().nextTracks(addCount);
            for (RetrowaveApi.TrackMeta item : meta) {
                long duration = (long) (item.duration * 1000);
                JsonObject data = new JsonObject();
                data.addProperty("streamUrl", item.getStreamUrl());
                playlist.entries.add(new Playlist.Entry(PROVIDER_RETROWAVE, item.title, duration, data));
            }
        }
    }

    public static Playlist.Entry oneRwPlease() throws IOException {
        RetrowaveApi.TrackMeta[] meta = new RetrowaveApi().nextTracks(1);
        RetrowaveApi.TrackMeta item = meta[0];

        long duration = (long) (item.duration * 1000);
        JsonObject data = new JsonObject();
        data.addProperty("streamUrl", item.getStreamUrl());

        return new Playlist.Entry(PROVIDER_RETROWAVE, item.title, duration, data);
    }

    private static final String YT_HASH_REGEX = "[a-zA-Z0-9\\-_]{11}";
    private static final String YT_HASH_URL = "https://youtube.com/watch?v=%s";

    public static Playlist.Entry youtube(String query) throws IOException {
        boolean isSearch;
        String ref;

        if (query.matches(YT_HASH_REGEX)) {
            ref = String.format(YT_HASH_URL, query);
            isSearch = false;
        } else if (query.startsWith("http") && query.contains("youtu")) {
            ref = query;
            isSearch = false;
        } else {
            ref = query;
            isSearch = true;
        }

        long duration = -1;
        JsonObject data = new JsonObject();
        data.addProperty("ref", ref);
        data.addProperty("search", isSearch);

        return new Playlist.Entry(PROVIDER_YOUTUBE, query, duration, data);
    }
}
