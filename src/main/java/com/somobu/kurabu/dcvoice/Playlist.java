package com.somobu.kurabu.dcvoice;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Playlist {

    public static class Entry {

        public int provider;
        public String title;
        public long duration;

        public JsonObject data;

        public Entry(int provider, String title, long duration, JsonObject data) {
            this.provider = provider;
            this.title = title;
            this.duration = duration;
            this.data = data;
        }
    }

    public final List<Entry> entries = Collections.synchronizedList(new ArrayList<Entry>());

    public int playlist_manager = Providers.LIST_MGR_NONE;

    public String[] simpleNames() {
        ArrayList<String> names = new ArrayList<>();

        synchronized (entries) {
            for (Entry entry : entries) {
                names.add(entry.title);
            }
        }

        return names.toArray(new String[0]);
    }
}
