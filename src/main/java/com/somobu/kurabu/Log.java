package com.somobu.kurabu;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Log {

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyy.MM.dd HH:mm:ss");

    public static void e(Exception e) {
        String output = e.getMessage();
        for (StackTraceElement el : e.getStackTrace()) output += "\n" + el.toString();
        d(output);
    }

    public static void d(String message) {
        StackTraceElement elmnt = new Exception().getStackTrace()[1];
        long time = System.currentTimeMillis();
        String tag = elmnt.getClassName() + ":" + elmnt.getLineNumber();
        d(tag, time, message);
    }

    public static void d(String tag, long time, String message) {
        System.err.println(sdf.format(new Date(time)) + " " + tag + " " + message);
    }

}
