package com.somobu.kurabu;

public class Necron extends Thread {

    private static final boolean DEBUG = false;

    public static final long FIRE_THRESHOLD_MS = 2 * 1000;

    public volatile boolean forceQuit = false;

    public void notifyTaskRecalc() {
        interrupt();
    }

    @Override
    public void run() {
        while (!forceQuit) {
            long currentTime = System.currentTimeMillis();
            long nextDiscordTime = Main.discord.handleNecron(currentTime);
            long nextIrcTime = Long.MAX_VALUE;

            long resultingSleepTime;
            long discordSleepTime = nextDiscordTime - System.currentTimeMillis();
            long ircSleepTime = nextIrcTime - System.currentTimeMillis();

            resultingSleepTime = Math.max(
                    Math.min(discordSleepTime, ircSleepTime),
                    FIRE_THRESHOLD_MS / 2
            );

            try {
                if (DEBUG) System.out.println("Necron: going to sleep " + (resultingSleepTime) + " ms");
                Thread.sleep(resultingSleepTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
