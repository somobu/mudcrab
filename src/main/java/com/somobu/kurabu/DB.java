package com.somobu.kurabu;

import java.sql.*;
import java.util.Calendar;

public class DB {

    private final Connection connection;

    public DB(String path) throws SQLException {
        connection = DriverManager.getConnection("jdbc:h2:" + path, "sa", "");

        // Outdated, not needed anymore
        dropTable("zavet_words");
        dropTable("zavet_links");
        dropTable("sudak");
        dropTable("log_records");

        // Actual tables, DO NOT CHANGE IT!!!
        checkOrCreateTable("words_total", "server text, uid text, uname text, cnt integer");
        checkOrCreateTable("words_by_day", "server text, day text, uid text, uname text, cnt integer");

        checkOrCreateTable("remindme",
                "_id integer primary key auto_increment, " +
                        "millis long, guild text, channel text, user_id text, " +
                        "message_txt text");
    }

    private void checkOrCreateTable(String name, String fields) throws SQLException {
        try {
            connection.createStatement().execute("SELECT 1 FROM " + name + " LIMIT 1;");
        } catch (Exception e) {
            System.out.println("Creating table " + name);
            connection.createStatement().execute("CREATE TABLE " + name + "(" + fields + ");");
        }
    }

    private void dropTable(String name) {
        try {
            connection.createStatement().execute("DROP TABLE " + name + ";");
            System.out.println("Dropping table " + name);
        } catch (Exception e) {

        }
    }

    private void execParametrized(String query, String... params) throws SQLException {
        PreparedStatement stmt = connection.prepareStatement(query);
        for (int i = 0; i < params.length; i++) {
            stmt.setString(i + 1, params[i]);
        }
        stmt.executeUpdate();

    }

    public synchronized void updateWordCounterTotal(String server, String user, String username, int cnt) {

        try {
            ResultSet rs = connection.createStatement().executeQuery("SELECT cnt FROM words_total WHERE uid = '" + user + "' AND server = '" + server + "'");
            if (rs.next()) {
                int total = cnt + rs.getInt(1);
                execParametrized("UPDATE words_total SET uname = ?, cnt = ? WHERE uid = ? AND server = ?", username, "" + total, user, server);
            } else {
                execParametrized("INSERT INTO words_total(server, uid, uname, cnt) VALUES (?, ?, ?, ?);", server, user, username, cnt + "");
            }
        } catch (SQLException e) {
            Main.e(e);
        }
    }

    public synchronized void updateWordCounterToday(String server, String user, String username, int cnt) {

        String key = getCurrentDayKey();

        try {
            ResultSet rs = connection.createStatement().executeQuery(
                    String.format("SELECT cnt FROM words_by_day WHERE uid = '%s' AND day = %s AND server = '%s'", user, key, server));
            if (rs.next()) {
                int total = cnt + rs.getInt(1);
                execParametrized("UPDATE words_by_day SET uname = ?, cnt = ? WHERE uid = ? AND day = ? AND server = ?", username, "" + total, user, key, server);
            } else {
                execParametrized("INSERT INTO words_by_day(server, day, uid, uname, cnt) VALUES (?, ?, ?, ?, ?);", server, key, user, username, cnt + "");
            }
        } catch (SQLException e) {
            Main.e(e);
        }
    }

    public String getPrettyWordCounterTop(String guild) {
        StringBuilder result = new StringBuilder();

        try {
            ResultSet rs = connection.createStatement().executeQuery(
                    String.format("SELECT uname, cnt FROM words_total WHERE server = '%s' ORDER BY cnt DESC LIMIT 10", guild));
            int i = 0;
            while (rs.next()) {
                i++;

                int count = rs.getInt(2);
                result.append(String.format("%d. **%s**: %d %s\n", i, rs.getString(1), count, formatWordWord(count)));
            }
        } catch (SQLException e) {
            Main.e(e);
        }

        return result.toString();
    }

    public String getPrettyWordCounterToday(String guild) {
        StringBuilder result = new StringBuilder();

        try {
            ResultSet rs = connection.createStatement().executeQuery(
                    String.format("SELECT uname, cnt FROM words_by_day WHERE day = %s AND server = '%s' ORDER BY cnt DESC LIMIT 10",
                            getCurrentDayKey(), guild));
            int i = 0;
            while (rs.next()) {
                i++;

                int count = rs.getInt(2);
                result.append(String.format("%d. **%s**: %d %s\n", i, rs.getString(1), count, formatWordWord(count)));
            }
        } catch (SQLException e) {
            Main.e(e);
        }

        return result.toString();
    }

    public static long getCurrentDayStart() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTimeInMillis();
    }

    private String getCurrentDayKey() {
        return getCurrentDayStart() + "";
    }

    private String formatWordWord(int cnt) {

        if (cnt >= 6 && cnt <= 20) return "слов";

        int lastDec = cnt % 10;
        if (lastDec < 1) return "слов";
        if (lastDec < 2) return "слово";
        if (lastDec < 5) return "слова";
        return "слов";
    }

    public void addReminder(long millis, String guild, String channel, String user, String text) throws SQLException {
        execParametrized("INSERT INTO remindme(millis, guild, channel, user_id, message_txt) " +
                        "VALUES (?, ?, ?, ?, ?);",
                "" + millis, guild, channel, user, text);
    }

    public static class Reminder {
        public long id;
        public long millis;
        public String guild;
        public String channel;
        public String user_id;
        public String message_txt;
    }

    public Reminder getNearestReminder(String guild, long after) throws SQLException {
        ResultSet rs = connection.createStatement().executeQuery(
                "SELECT _id, millis, guild, channel, user_id, message_txt FROM remindme " +
                        "WHERE guild = " + guild + " AND millis > " + after + " " +
                        "ORDER BY millis ASC " +
                        "LIMIT 1");
        if (rs.next()) {
            Reminder reminder = new Reminder();
            reminder.id = rs.getLong(1);
            reminder.millis = rs.getLong(2);
            reminder.guild = rs.getString(3);
            reminder.channel = rs.getString(4);
            reminder.user_id = rs.getString(5);
            reminder.message_txt = rs.getString(6);
            return reminder;
        } else {
            return null;
        }
    }

    public void removeReminder(long id) throws SQLException {
        execParametrized("DELETE FROM remindme WHERE _id = ?", "" + id);
    }
}
