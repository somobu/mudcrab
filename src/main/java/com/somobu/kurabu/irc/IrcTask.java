package com.somobu.kurabu.irc;

import com.somobu.kurabu.Task;

public class IrcTask extends Task {

    // ============ Scheduled tasks ============ //

    @Override
    public final void onScheduled(Object[] data) {

    }

    // ========== Realtime api events ========== //

    public void onUserJoin(String channel, String login, String hostname) {

    }

    public void onUserLeft(String login, String hostname, String reason) {

    }

    public void onUserMessage(String channel, String login, String hostname, String message) {

    }

}
